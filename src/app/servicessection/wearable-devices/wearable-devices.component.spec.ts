import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WearableDevicesComponent } from './wearable-devices.component';

describe('WearableDevicesComponent', () => {
  let component: WearableDevicesComponent;
  let fixture: ComponentFixture<WearableDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WearableDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WearableDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
