import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IphoneAppDevelopmentComponent } from './iphone-app-development.component';

describe('IphoneAppDevelopmentComponent', () => {
  let component: IphoneAppDevelopmentComponent;
  let fixture: ComponentFixture<IphoneAppDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IphoneAppDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IphoneAppDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
