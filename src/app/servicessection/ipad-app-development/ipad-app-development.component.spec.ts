import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IpadAppDevelopmentComponent } from './ipad-app-development.component';

describe('IpadAppDevelopmentComponent', () => {
  let component: IpadAppDevelopmentComponent;
  let fixture: ComponentFixture<IpadAppDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IpadAppDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpadAppDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
