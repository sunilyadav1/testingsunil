import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackberryAppDevelopmentComponent } from './blackberry-app-development.component';

describe('BlackberryAppDevelopmentComponent', () => {
  let component: BlackberryAppDevelopmentComponent;
  let fixture: ComponentFixture<BlackberryAppDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackberryAppDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackberryAppDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
