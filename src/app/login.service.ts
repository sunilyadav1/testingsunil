import {Injectable} from '@angular/core';
import { Http,Headers, URLSearchParams}  from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {


  
 constructor(public _http: Http) { }


 
 private _contactUrl = 'http://www.bigperl.com/webservices/index.php/Api/contact';
 
 
  sendLogin(value:any){
   

    
    const body = new URLSearchParams(value);
    body.set('name', value.data[0].name);
    body.set('email', value.data[0].email);
    body.set('subject', value.data[0].subject);
    body.set('message', value.data[0].message);
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this._http.post(this._contactUrl, body, {
      headers: headers
    }).map( res => res.json() );
   
  }
}