import { FormsModule } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import {NgForm} from '@angular/forms';
import { ToasterService } from './../toaster-service.service';


declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
//   providers: [LoginServices]
})
export class FooterComponent implements OnInit {
    name: string = "";
    email: string = "";
    subject: string = "";
    message: string = "";
    
    

  constructor(public router: Router, public _loginService: LoginService, private toasterService: ToasterService) { }


  Success(){
    this.toasterService.Success("Your message has been sent successfully.")
  }
  Info(){
    this.toasterService.Info("Subscribed")
  }
  ngOnInit() {

    $(document).ready(function() {
      $(".position5").hide();
      $(".position2").show();
      $('.position2').click(function() {
       $(".position5").slideToggle();
      });
  });
  
  $(".waves-circle i").click(function() {
    $(this).text(function(i, text) {
        return text === "close" ? "comment" : "close"
    })
});
(function($) {
    $('div.whatsappme__button1').click(function() {
        $('div.whatsappme__box1').show()
    });
    $('#message_close').click(function() {
        $('.whatsappme__box1').hide()
    })
}(jQuery))
  }



  login() {
    let user = new User('', '','','');
    user.name = this.name;
    user.email = this.email;
     user.subject = this.subject;
    user.message = this.message;

    let data = [
      { 'name': user.name , 'email': user.email ,'subject': user.subject, 'message': user.message }
    ];
    this._loginService.sendLogin({ data })
      .subscribe(
      response => this.handleResponse(response),
      error => this.handleResponse(error),
      $('.whatsappme__box1').hide()
      );
      
      

      
  }

  handleResponse(response) {
    if (response.success) {
      console.log("success");
    } else if (response.error) {
      console.log("errror");
    } else {

    }

  }

}
