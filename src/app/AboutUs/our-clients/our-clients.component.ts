import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-our-clients',
  templateUrl: './our-clients.component.html',
  styleUrls: ['./our-clients.component.css']
})
export class OurClientsComponent implements OnInit {

  constructor() { }

  ngOnInit() {


    $('#customers-carousel3').owlCarousel({responsive:{0:{items:1},600:{items:3},1000:{items:4}},loop:true,margin:30,nav:true,smartSpeed:2000,navText:["<i class='fa fa-chevron-left position'></i>","<i class='fa fa-chevron-right position1'></i>"],autoplay:true,autoplayTimeout:3000});
  }

}
