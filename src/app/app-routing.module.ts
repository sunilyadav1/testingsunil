import { HowIndexingAndDeepLinkingHelpComponent } from './components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component';
import { LocationBasedTechnologyFormobileAppComponent } from './components/location-based-technology-formobile-app/location-based-technology-formobile-app.component';
import { XamarinDevelopmentComponent } from './components/xamarin-development/xamarin-development.component';






import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import {PortfolioComponent } from './portfolio/portfolio.component';


import { PortfolioSinglepageComponent } from './portfolio/portfolio-singlepage/portfolio-singlepage.component';
import {CareersComponent } from './careers/careers.component';
import {AndroidAppDevelopmentComponent } from './servicessection/android-app-development/android-app-development.component';
import {IphoneAppDevelopmentComponent } from './servicessection/iphone-app-development/iphone-app-development.component';
import {IpadAppDevelopmentComponent } from './servicessection/ipad-app-development/ipad-app-development.component';
import { AdfDevelopmentComponent } from './servicessection/adf-development/adf-development.component';
import { AdfWebcenterDevelopmentComponent } from './servicessection/adf-webcenter-development/adf-webcenter-development.component';
import { BlackberryAppDevelopmentComponent } from './servicessection/blackberry-app-development/blackberry-app-development.component';
import { CakePhpDevelopmentComponent } from './servicessection/cake-php-development/cake-php-development.component';
import { FacebookAppDevelopmentComponent } from './servicessection/facebook-app-development/facebook-app-development.component';
import { EcommerceDevelopmentComponent } from './servicessection/ecommerce-development/ecommerce-development.component';
import { EcommerceDeveloperComponent } from './servicessection/ecommerce-developer/ecommerce-developer.component';
import { DrupalDevelopmentComponent } from './servicessection/drupal-development/drupal-development.component';
import { CodeigniterDevelopmentComponent } from './servicessection/codeigniter-development/codeigniter-development.component';
import { CmcDevelopmentComponent } from './servicessection/cmc-development/cmc-development.component';
import { LaravelDevelopmentComponent } from './servicessection/laravel-development/laravel-development.component';
import { JoomlaDevelopmentComponent } from './servicessection/joomla-development/joomla-development.component';
import { JavaJ2eeDevelopmentComponent } from './servicessection/java-j2ee-development/java-j2ee-development.component';
import { InternetOfThingsComponent } from './servicessection/internet-of-things/internet-of-things.component';
import { HybridAppDevelopmentComponent } from './servicessection/hybrid-app-development/hybrid-app-development.component';
import { PythonWebDevelopmentComponent } from './servicessection/python-web-development/python-web-development.component';
import { PythonDeveloperComponent } from './servicessection/python-developer/python-developer.component';
import { PhpDevelopmentComponent } from './servicessection/php-development/php-development.component';
import { PhpDeveloperComponent } from './servicessection/php-developer/php-developer.component';
import { PhoneGapDevelopmentComponent } from './servicessection/phone-gap-development/phone-gap-development.component';
import { MagentoDevelopmentComponent } from './servicessection/magento-development/magento-development.component';
import { MagentoDeveloperComponent } from './servicessection/magento-developer/magento-developer.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { BlogsComponent } from './AboutUs/blogs/blogs.component';
import { OurClientsComponent } from './AboutUs/our-clients/our-clients.component';
import { SucessStoriesComponent } from './AboutUs/sucess-stories/sucess-stories.component';
import { IosDeveloperComponent } from './servicessection/ios-developer/ios-developer.component';
import { AndroidDeveloperComponent } from './servicessection/android-developer/android-developer.component';
import { XCartDevelopmentComponent } from './servicessection/xcart-development/xcart-development.component';
import { WordPressDevelopmentComponent } from './servicessection/word-press-development/word-press-development.component';
import { WindowsAppDevelopmentComponent } from './servicessection/windows-app-development/windows-app-development.component';
import { WebApplicationDevelopmentComponent } from './servicessection/web-application-development/web-application-development.component';
import { WearableDevicesComponent } from './servicessection/wearable-devices/wearable-devices.component';
import { RubyOnRailsDevelopmentComponent } from './servicessection/ruby-on-rails-development/ruby-on-rails-development.component';
import { RubyOnRailsDeveloperComponent } from './servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component';
import { ResponsiveWebDevelopmentComponent } from './servicessection/responsive-web-development/responsive-web-development.component';
import { TravelBookingEcommerceComponent } from './Products/travel-booking-ecommerce/travel-booking-ecommerce.component';
import { SassBasedPayrollComponent } from './Products/sass-based-payroll/sass-based-payroll.component';
import { SassBasedErpComponent } from './Products/sass-based-erp/sass-based-erp.component';
import { HotelManagementSystemComponent } from './Products/hotel-management-system/hotel-management-system.component';
import { HotelInventoryManagementComponent } from './Products/hotel-inventory-management/hotel-inventory-management.component';
import { HotelBookingEngineComponent } from './Products/hotel-booking-engine/hotel-booking-engine.component';
import { GpsWearablesComponent } from './Products/gps-wearables/gps-wearables.component';
import { GpsVehicleTrakingComponent } from './Products/gps-vehicle-traking/gps-vehicle-traking.component';
import { GpsMobileTrakingComponent } from './Products/gps-mobile-traking/gps-mobile-traking.component';
import { GpsAssetsTrakingComponent } from './Products/gps-assets-traking/gps-assets-traking.component';
import { FoodOnWheelComponent } from './Products/food-on-wheel/food-on-wheel.component';
import { FieldForceAutomationComponent } from './Products/field-force-automation/field-force-automation.component';
import { EnterpriceCrmSupportComponent } from './Products/enterprice-crm-support/enterprice-crm-support.component';
import { EnterpriceCrmSalesComponent } from './Products/enterprice-crm-sales/enterprice-crm-sales.component';
import { ConstructionSuppliersComponent } from './Products/construction-suppliers/construction-suppliers.component';
import { ArchitechturalNetworkComponent } from './Products/architechtural-network/architechtural-network.component';
import { LifeScienceAndHealthCareComponent } from './Industry/life-science-and-health-care/life-science-and-health-care.component';
import { TravelTransportLogComponent } from './Industry/travel-transport-log/travel-transport-log.component';
import { TelecomComponent } from './Industry/telecom/telecom.component';
import { SocialComputingComponent } from './Industry/social-computing/social-computing.component';
import { RetailAndConsumerComponent } from './Industry/retail-and-consumer/retail-and-consumer.component';
import { RealEstateComponent } from './Industry/real-estate/real-estate.component';
import { MediaEntertainmentComponent } from './Industry/media-entertainment/media-entertainment.component';
import { ManufacturingComponent } from './Industry/manufacturing/manufacturing.component';
import { ItComponent } from './Industry/it/it.component';
import { HospitalityComponent } from './Industry/hospitality/hospitality.component';
import { GovernmentComponent } from './Industry/government/government.component';
import { EducationComponent } from './Industry/education/education.component';
import { DigitalMarketingComponent } from './Industry/digital-marketing/digital-marketing.component';
import { ConsumerElectronicsComponent } from './Industry/consumer-electronics/consumer-electronics.component';
import { BankingAndFinancialComponent } from './Industry/banking-and-financial/banking-and-financial.component';
import { AutomotiveComponent } from './Industry/automotive/automotive.component';


import { Home1Component } from './home1/home1.component';
import { HomeComponent } from './home/home.component';



const appRoutes: Routes =[
      {
            path: '',
            component: Home1Component,
          },
          {
            path: 'home',
            component: Home1Component,
          },

      {path: 'portfolio', component: PortfolioComponent },
      
      {path: 'portfolio-singlepage', component: PortfolioSinglepageComponent },
      {path: 'careers', component: CareersComponent },
      {path: 'android-app-development', component: AndroidAppDevelopmentComponent },
      {path: 'android-developer', component: AndroidDeveloperComponent },
      {path: 'iphone-app-development', component: IphoneAppDevelopmentComponent },
      {path: 'ipad-app-development', component: IpadAppDevelopmentComponent },
      {path: 'ios-developer', component: IosDeveloperComponent },
      {path: 'adf-developer', component: AdfDevelopmentComponent },
      {path: 'adf-and-web-development', component: AdfWebcenterDevelopmentComponent },
      {path: 'blackberry-app-development', component: BlackberryAppDevelopmentComponent },
      {path: 'cake-php-development', component: CakePhpDevelopmentComponent },
      {path: 'cmc-development', component: CmcDevelopmentComponent },
      {path: 'codeigniter-development', component: CodeigniterDevelopmentComponent },
      {path: 'drupal-development', component: DrupalDevelopmentComponent },
      {path: 'ecommerce-developer', component: EcommerceDeveloperComponent },
      {path: 'e-commerce-development', component: EcommerceDevelopmentComponent },
      {path: 'facebook-app-development', component: FacebookAppDevelopmentComponent },
      {path: 'hybrid-app-development', component: HybridAppDevelopmentComponent },
      {path: 'internet-of-things', component: InternetOfThingsComponent },
      {path: 'java-&-j2ee-development', component: JavaJ2eeDevelopmentComponent },
      {path: 'joomla-development', component: JoomlaDevelopmentComponent },
      {path: 'laravel-development', component: LaravelDevelopmentComponent },
      {path: 'magento-developer', component: MagentoDeveloperComponent },
      {path: 'magento-development', component: MagentoDevelopmentComponent },
      {path: 'phone-gap-development', component: PhoneGapDevelopmentComponent },
      {path: 'php-developer', component: PhpDeveloperComponent },
      {path: 'php-development', component: PhpDevelopmentComponent },
      {path: 'python-developer', component: PythonDeveloperComponent },
      {path: 'python-web-development', component: PythonWebDevelopmentComponent },
      {path: 'responsive-web-development', component: ResponsiveWebDevelopmentComponent },
      {path: 'ruby-on-rails-developer', component: RubyOnRailsDeveloperComponent },
      {path: 'ruby-on-rails-development', component: RubyOnRailsDevelopmentComponent },
      {path: 'wearable-devices', component: WearableDevicesComponent },
      {path: 'web-application-development', component: WebApplicationDevelopmentComponent },
      {path: 'windows-app-development', component: WindowsAppDevelopmentComponent },
      {path: 'word-press-development', component: WordPressDevelopmentComponent },
      {path: 'xcart-development', component: XCartDevelopmentComponent },
      {path: 'success-stories', component: SucessStoriesComponent },
      {path: 'our-clients', component: OurClientsComponent },
      {path: 'blogs', component: BlogsComponent },
      {path: 'contact-us', component: ContactUsComponent },
      {path: 'automative', component: AutomotiveComponent },
      {path: 'banking-&-financial', component: BankingAndFinancialComponent },
      {path: 'consumer-electronics', component: ConsumerElectronicsComponent },
      {path: 'digital-marketing', component: DigitalMarketingComponent },
      {path: 'education', component: EducationComponent },
      {path: 'government', component: GovernmentComponent },
      {path: 'hospitality', component: HospitalityComponent },
      {path: 'it', component: ItComponent },
      {path: 'manufacturing', component: ManufacturingComponent },
      {path: 'media-entertainment', component: MediaEntertainmentComponent },
      {path: 'real-estate', component: RealEstateComponent },
      {path: 'retail-and-consumer', component: RetailAndConsumerComponent },
      {path: 'social-computing', component: SocialComputingComponent },
      {path: 'telecom', component: TelecomComponent },
      {path: 'travel-transport-&-log', component: TravelTransportLogComponent },
      {path: 'life-science-&-healthcare', component: LifeScienceAndHealthCareComponent },
      {path: 'architechtural-network', component: ArchitechturalNetworkComponent },
      {path: 'construction-suppliers', component: ConstructionSuppliersComponent },
      {path: 'enterprice-crm-sales', component: EnterpriceCrmSalesComponent },
      {path: 'enterprice-crm-support', component: EnterpriceCrmSupportComponent },
      {path: 'field-force-automation', component: FieldForceAutomationComponent },
      {path: 'food-on-wheel', component: FoodOnWheelComponent },
      {path: 'gps-assets-traking', component: GpsAssetsTrakingComponent },
      {path: 'gps-mobile-traking', component: GpsMobileTrakingComponent },
      {path: 'gps-vehicle-traking', component: GpsVehicleTrakingComponent },
      {path: 'gps-wearables', component: GpsWearablesComponent },
      {path: 'hotel-booking-engine', component: HotelBookingEngineComponent },
      {path: 'hotel-inventory-management', component: HotelInventoryManagementComponent },
      {path: 'hotel-management-system', component: HotelManagementSystemComponent },
      {path: 'sass-based-erp', component: SassBasedErpComponent },
      {path: 'sass-based-payroll', component: SassBasedPayrollComponent },
      {path: 'travel-booking-e-commerce', component: TravelBookingEcommerceComponent },
      {path: 'xamarin-development', component: XamarinDevelopmentComponent },
      {path: 'location-based-technology-formobile-app', component: LocationBasedTechnologyFormobileAppComponent },
      {path: 'how-indexing-and-deep-linking-help', component: HowIndexingAndDeepLinkingHelpComponent },
      
      
      {
            path: '**',
            redirectTo: '',
          },
    
];

@NgModule({
 imports :[RouterModule.forRoot(appRoutes)],
 exports: [RouterModule]
})
export class AppRoutingModule {

}