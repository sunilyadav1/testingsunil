import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import { ToasterService } from './../toaster-service.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  name: string = "";
  email: string = "";
  subject: string = "";
  message: string = "";

  constructor(public router: Router, public _loginService: LoginService, private toasterService: ToasterService) { }
  Success(){this.toasterService.Success("Your message has been sent successfully.")}
Info(){this.toasterService.Info("Subscribed")}

ngOnInit(){}

login() {
  let user = new User('', '', '', '');
  user.name = this.name;
  user.email = this.email;
  user.subject = this.subject;
  user.message = this.message;
  let data = [{
      'name': user.name,
      'email': user.email,
      'subject': user.subject,
      'message': user.message
  }];
  this._loginService.sendLogin({
      data
  }).subscribe(response => this.handleResponse(response), error => this.handleResponse(error))
}
handleResponse(response) {
  if (response.success) {
      console.log("success")
  } else if (response.error) {
      console.log("errror")
  } else {}
}

}
