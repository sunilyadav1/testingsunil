import { Component, OnInit } from '@angular/core';



declare var $: any;
declare var jQuery :any;

declare var imagesLoaded : any;
@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  constructor() { 


    
  }

  ngOnInit() {

    "use strict";$(document).ready(function(){$(".portfolio-container").each(function(e,t){var i=$(this).find(".portfolio"),o=this;i.shuffle({itemSelector:".portfolio-item"}),$(this).find(".portfolio-filter li").on("click",function(e){e.preventDefault(),$(o).find(".portfolio-filter li").removeClass("active"),$(this).addClass("active");var t=$(this).attr("data-group");i.shuffle("shuffle",t)})}),$(".portfolio-slider").length>0&&$(".portfolio-wrapper").each(function(e,t){var i=$(this).find(".portfolio-slider"),o=i.attr("data-direction");i.flexslider({animation:"slide",direction:o,slideshowSpeed:3e3,start:function(){imagesLoaded($(".portfolio"),function(){setTimeout(function(){$(".portfolio-filter li:eq(0)").trigger("click")},500)})}})}),$(".portfolio-slider").each(function(){for(var e=$(this).find("li > a"),t=[],i=0;i<e.length;i++)t.push({src:$(e[i]).attr("href"),title:$(e[i]).attr("title")});$(this).parent().find(".action-btn").magnificPopup({items:t,type:"image",mainClass:"mfp-fade",removalDelay:160,fixedContentPos:!1,gallery:{enabled:!0}})})})



  }

}
