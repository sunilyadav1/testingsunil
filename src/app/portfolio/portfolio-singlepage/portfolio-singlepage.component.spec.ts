import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioSinglepageComponent } from './portfolio-singlepage.component';

describe('PortfolioSinglepageComponent', () => {
  let component: PortfolioSinglepageComponent;
  let fixture: ComponentFixture<PortfolioSinglepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioSinglepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioSinglepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
