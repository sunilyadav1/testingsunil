import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import { ToasterService } from './../toaster-service.service';



declare var materialInit: any;

declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css']
})
export class CareersComponent implements OnInit {
  name: string = "";
    email: string = "";
    subject: string = "";
    message: string = "";

   
  constructor(public router: Router, public _loginService: LoginService,private toasterService: ToasterService) { }

  

  ngOnInit(){

    this.jquery_code();
  }


  jquery_code(){

    $(function() {
      //----- OPEN
      $('[data-popup-open]').on('click', function(e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
    
        e.preventDefault();
      });
    
      //----- CLOSE
      $('[data-popup-close]').on('click', function(e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
    
        e.preventDefault();
      });
    });
    
         
  }






  Success(){this.toasterService.Success("Your message has been sent successfully.")}
  Info(){this.toasterService.Info("Subscribed")}


 
  login(){let user=new User('','','','');user.name=this.name;user.email=this.email;user.subject=this.subject;user.message=this.message;let data=[{'name':user.name,'email':user.email,'subject':user.subject,'message':user.message}];this._loginService.sendLogin({data}).subscribe(response=>this.handleResponse(response),error=>this.handleResponse(error))}
  handleResponse(response){if(response.success){console.log("success")}else if(response.error){console.log("errror")}else{}}
   materialInit = function () {

   ;
    }

}

  