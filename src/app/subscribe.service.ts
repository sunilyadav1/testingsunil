import {Injectable}               from '@angular/core';
import { Http,Headers, URLSearchParams}  from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class SubscribeService {


  
 constructor(public _http: Http) { }


 
 private _contactUrl1 = 'http://www.bigperl.com/webservices/index.php/Api/subscribe';
 sendSubscribe(value:any){
   

    
    const body = new URLSearchParams(value);
  
    body.set('email', value.data[0].email);
   
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this._http.post(this._contactUrl1, body, {
      
      headers: headers
    }).map( res => res.json() );
   
  }
}