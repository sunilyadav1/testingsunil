import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';


declare var $: any;
@Component({
  selector: 'app-footer1',
  templateUrl: './footer1.component.html',
  styleUrls: ['./footer1.component.css']
})
export class Footer1Component implements OnInit {
  name: string = "";
  email: string = "";
  subject: string = "";
  message: string = "";
  constructor(public router: Router, public _loginService: LoginService) { }

  ngOnInit() {


  }
  login() {
    let user = new User('', '','','');
    user.name = this.name;
    user.email = this.email;
     user.subject = this.subject;
    user.message = this.message;

    let data = [
      { 'name': user.name , 'email': user.email ,'subject': user.subject, 'message': user.message }
    ];
    this._loginService.sendLogin({ data })
      .subscribe(
      response => this.handleResponse(response),
      error => this.handleResponse(error)
      );
    
  }

  handleResponse(response) {
    if (response.success) {
      console.log("success");
    } else if (response.error) {
      console.log("errror");
    } else {

    }

  }
}
