import { SubscribeService } from './../subscribe.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subscribe } from '../subscribe.model';
import { ToasterService } from './../toaster-service.service';
declare var $: any;
@Component({
  selector: 'app-footer4',
  templateUrl: './footer4.component.html',
  styleUrls: ['./footer4.component.css']
})
export class Footer4Component implements OnInit {
  email: string = "";
  constructor(public router: Router,  public _subscribeService: SubscribeService,private toasterService: ToasterService) { }


  Success(){
    this.toasterService.Success("Your message has been sent successfully.")
  }
  Info(){
    this.toasterService.Info("Subscribed")
  }

  ngOnInit() {

// ===== Scroll to Top ==== 
$(window).scroll(function(){if($(this).scrollTop()>=50){$('#return-to-top').fadeIn(200)}else{$('#return-to-top').fadeOut(200)}});$('#return-to-top').click(function(){$('body,html').animate({scrollTop:0},500)})
    






  }


  subscribe() {
    let subscribe = new Subscribe('');
   
    subscribe.email = this.email;
    

    let data = [
      {  'email': subscribe.email  }
    ];
    this._subscribeService.sendSubscribe({ data })
      .subscribe(
      response => this.handleResponse(response),
      error => this.handleResponse(error)
      );




      
    console.log(data);
  }

  handleResponse(response) {
    if (response.success) {
      console.log("success");
    } else if (response.error) {
      console.log("errror");
    } else {

    }

  }

}
