import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import { Subscribe } from '../subscribe.model';
import { SubscribeService } from './../subscribe.service';
import { ToasterService } from './../toaster-service.service';


declare var $: any;


declare var List :any;



@Component({
  selector: 'app-footer2',
  templateUrl: './footer2.component.html',
  styleUrls: ['./footer2.component.css']
})
export class Footer2Component implements OnInit {
    name: string = "";
    email: string = "";
    subject: string = "";
    message: string = "";
    constructor(public router:Router,public _loginService:LoginService,public _subscribeService:SubscribeService,private toasterService:ToasterService){}
    Success(){this.toasterService.Success("Thank you! Your message has been sent successfully.")}
    Info(){this.toasterService.Info("Subscribed")}
    ngOnInit(){function myFunction(){var input,filter,ul,li,a,i;input=document.getElementById("myInput");filter=input.value.toUpperCase();ul=document.getElementById("myUL");li=ul.getElementsByTagName("li");for(i=0;i<li.length;i++){a=li[i].getElementsByTagName("a")[0];if(a.innerHTML.toUpperCase().indexOf(filter)>-1){li[i].style.display=""}else{li[i].style.display="none"}}}
    $('#customers-carousel1').owlCarousel({responsive:{0:{items:1},600:{items:3},1000:{items:4}},loop:!0,margin:30,nav:!0,smartSpeed:900,navText:["<i class='fa fa-chevron-left position'></i>","<i class='fa fa-chevron-right position1'></i>"],autoplay:!0,autoplayTimeout:3000});$(document).ready(function(){$(".position5").hide();$(".position2").show();$('.position2').click(function(){$(".position5").slideToggle()})});$(".waves-circle i").click(function(){$(this).text(function(i,text){return text==="close"?"comment":"close"})});var monkeyList=new List('test-list',{valueNames:['name'],page:6,pagination:!0})}
    login(){let user=new User('','','','');user.name=this.name;user.email=this.email;user.subject=this.subject;user.message=this.message;let data=[{'name':user.name,'email':user.email,'subject':user.subject,'message':user.message}];this._loginService.sendLogin({data}).subscribe(response=>this.handleResponse(response),error=>this.handleResponse(error))}
    subscribe(){let subscribe=new Subscribe('');subscribe.email=this.email;let data=[{'email':subscribe.email}];this._subscribeService.sendSubscribe({data}).subscribe(response=>this.handleResponse(response),error=>this.handleResponse(error))}
    handleResponse(response){if(response.success){console.log("success")}else if(response.error){console.log("errror")}else{}}


}
