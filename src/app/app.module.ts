import { ToasterService } from './toaster-service.service';
import { SubscribeService } from './subscribe.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { CareersComponent } from './careers/careers.component';

import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ServicessectionComponent } from './servicessection/servicessection.component';
import { AndroidAppDevelopmentComponent } from './servicessection/android-app-development/android-app-development.component';
import { IphoneAppDevelopmentComponent } from './servicessection/iphone-app-development/iphone-app-development.component';
import { IpadAppDevelopmentComponent } from './servicessection/ipad-app-development/ipad-app-development.component';
import { WindowsAppDevelopmentComponent } from './servicessection/windows-app-development/windows-app-development.component';
import { BlackberryAppDevelopmentComponent } from './servicessection/blackberry-app-development/blackberry-app-development.component';
import { HybridAppDevelopmentComponent } from './servicessection/hybrid-app-development/hybrid-app-development.component';
import { WearableDevicesComponent } from './servicessection/wearable-devices/wearable-devices.component';
import { InternetOfThingsComponent } from './servicessection/internet-of-things/internet-of-things.component';
import { PhpDevelopmentComponent } from './servicessection/php-development/php-development.component';
import { RarDevelopmentComponent } from './servicessection/rar-development/rar-development.component';
import { JavaJ2eeDevelopmentComponent } from './servicessection/java-j2ee-development/java-j2ee-development.component';
import { ResponsiveWebDevelopmentComponent } from './servicessection/responsive-web-development/responsive-web-development.component';
import { PythonWebDevelopmentComponent } from './servicessection/python-web-development/python-web-development.component';
import { AdfWebcenterDevelopmentComponent } from './servicessection/adf-webcenter-development/adf-webcenter-development.component';
import { FacebookAppDevelopmentComponent } from './servicessection/facebook-app-development/facebook-app-development.component';
import { CodeigniterDevelopmentComponent } from './servicessection/codeigniter-development/codeigniter-development.component';
import { CakePhpDevelopmentComponent } from './servicessection/cake-php-development/cake-php-development.component';
import { LaravelDevelopmentComponent } from './servicessection/laravel-development/laravel-development.component';
import { CmcDevelopmentComponent } from './servicessection/cmc-development/cmc-development.component';
import { WordPressDevelopmentComponent } from './servicessection/word-press-development/word-press-development.component';
import { DrupalDevelopmentComponent } from './servicessection/drupal-development/drupal-development.component';
import { JoomlaDevelopmentComponent } from './servicessection/joomla-development/joomla-development.component';
import { EcommerceDevelopmentComponent } from './servicessection/ecommerce-development/ecommerce-development.component';
import { MagentoDevelopmentComponent } from './servicessection/magento-development/magento-development.component';
import { XCartDevelopmentComponent } from './servicessection/xcart-development/xcart-development.component';
import { WebApplicationDevelopmentComponent } from './servicessection/web-application-development/web-application-development.component';
import { PhoneGapDevelopmentComponent } from './servicessection/phone-gap-development/phone-gap-development.component';
import { AdfDevelopmentComponent } from './servicessection/adf-development/adf-development.component';
import { EcommerceDeveloperComponent } from './servicessection/ecommerce-developer/ecommerce-developer.component';
import { Home1Component } from './home1/home1.component';
import { Footer3Component } from './footer3/footer3.component';
import { FooterComponent } from './footer/footer.component';
import { Footer4Component } from './footer4/footer4.component';
import { Footer5Component } from './footer5/footer5.component';
import { Footer6Component } from './footer6/footer6.component';
import { Footer2Component } from './footer2/footer2.component';
import { MagentoDeveloperComponent } from './servicessection/magento-developer/magento-developer.component';
import { PhpDeveloperComponent } from './servicessection/php-developer/php-developer.component';
import { PythonDeveloperComponent } from './servicessection/python-developer/python-developer.component';
import { Footer1Component } from './footer1/footer1.component';
import { RubyOnRailsDeveloperComponent } from './servicessection/ruby-on-rails-developer/ruby-on-rails-developer.component';
import { RubyOnRailsDevelopmentComponent } from './servicessection/ruby-on-rails-development/ruby-on-rails-development.component';
import { AndroidDeveloperComponent } from './servicessection/android-developer/android-developer.component';
import { IosDeveloperComponent } from './servicessection/ios-developer/ios-developer.component';
import { OurClientsComponent } from './AboutUs/our-clients/our-clients.component';
import { SucessStoriesComponent } from './AboutUs/sucess-stories/sucess-stories.component';
import { BlogsComponent } from './AboutUs/blogs/blogs.component';
import { AutomotiveComponent } from './Industry/automotive/automotive.component';
import { DigitalMarketingComponent } from './Industry/digital-marketing/digital-marketing.component';
import { HospitalityComponent } from './Industry/hospitality/hospitality.component';
import { MediaEntertainmentComponent } from './Industry/media-entertainment/media-entertainment.component';
import { RealEstateComponent } from './Industry/real-estate/real-estate.component';
import { SocialComputingComponent } from './Industry/social-computing/social-computing.component';
import { BankingAndFinancialComponent } from './Industry/banking-and-financial/banking-and-financial.component';
import { EducationComponent } from './Industry/education/education.component';
import { ItComponent } from './Industry/it/it.component';
import { ManufacturingComponent } from './Industry/manufacturing/manufacturing.component';
import { TelecomComponent } from './Industry/telecom/telecom.component';
import { ConsumerElectronicsComponent } from './Industry/consumer-electronics/consumer-electronics.component';
import { GovernmentComponent } from './Industry/government/government.component';
import { RetailAndConsumerComponent } from './Industry/retail-and-consumer/retail-and-consumer.component';
import { TravelTransportLogComponent } from './Industry/travel-transport-log/travel-transport-log.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LifeScienceAndHealthCareComponent } from './Industry/life-science-and-health-care/life-science-and-health-care.component';
import { GpsMobileTrakingComponent } from './Products/gps-mobile-traking/gps-mobile-traking.component';
import { GpsVehicleTrakingComponent } from './Products/gps-vehicle-traking/gps-vehicle-traking.component';
import { GpsWearablesComponent } from './Products/gps-wearables/gps-wearables.component';
import { FieldForceAutomationComponent } from './Products/field-force-automation/field-force-automation.component';
import { GpsAssetsTrakingComponent } from './Products/gps-assets-traking/gps-assets-traking.component';
import { HotelBookingEngineComponent } from './Products/hotel-booking-engine/hotel-booking-engine.component';
import { HotelManagementSystemComponent } from './Products/hotel-management-system/hotel-management-system.component';
import { HotelInventoryManagementComponent } from './Products/hotel-inventory-management/hotel-inventory-management.component';
import { TravelBookingEcommerceComponent } from './Products/travel-booking-ecommerce/travel-booking-ecommerce.component';
import { FoodOnWheelComponent } from './Products/food-on-wheel/food-on-wheel.component';
import { ArchitechturalNetworkComponent } from './Products/architechtural-network/architechtural-network.component';
import { ConstructionSuppliersComponent } from './Products/construction-suppliers/construction-suppliers.component';
import { EnterpriceCrmSalesComponent } from './Products/enterprice-crm-sales/enterprice-crm-sales.component';
import { EnterpriceCrmSupportComponent } from './Products/enterprice-crm-support/enterprice-crm-support.component';
import { SassBasedErpComponent } from './Products/sass-based-erp/sass-based-erp.component';
import { SassBasedPayrollComponent } from './Products/sass-based-payroll/sass-based-payroll.component';
import { XamarinDevelopmentComponent } from './components/xamarin-development/xamarin-development.component';
import { LocationBasedTechnologyFormobileAppComponent } from './components/location-based-technology-formobile-app/location-based-technology-formobile-app.component';
import { HowIndexingAndDeepLinkingHelpComponent } from './components/how-indexing-and-deep-linking-help/how-indexing-and-deep-linking-help.component';


import { LoginService } from './login.service';
import { PortfolioSinglepageComponent } from './portfolio/portfolio-singlepage/portfolio-singlepage.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    PortfolioComponent,
    CareersComponent,
    ServicessectionComponent,
    AndroidAppDevelopmentComponent,
    IphoneAppDevelopmentComponent,
    IpadAppDevelopmentComponent,
    WindowsAppDevelopmentComponent,
    BlackberryAppDevelopmentComponent,
    HybridAppDevelopmentComponent,
    WearableDevicesComponent,
    InternetOfThingsComponent,
    PhpDevelopmentComponent,
    RarDevelopmentComponent,
    JavaJ2eeDevelopmentComponent,
    ResponsiveWebDevelopmentComponent,
    PythonWebDevelopmentComponent,
    AdfWebcenterDevelopmentComponent,
    FacebookAppDevelopmentComponent,
    CodeigniterDevelopmentComponent,
    CakePhpDevelopmentComponent,
    LaravelDevelopmentComponent,
    CmcDevelopmentComponent,
    WordPressDevelopmentComponent,
    DrupalDevelopmentComponent,
    JoomlaDevelopmentComponent,
    EcommerceDevelopmentComponent,
    MagentoDevelopmentComponent,
    XCartDevelopmentComponent,
    WebApplicationDevelopmentComponent,
    PhoneGapDevelopmentComponent,
    AdfDevelopmentComponent,
    EcommerceDeveloperComponent,
    Home1Component,
    Footer3Component,
    FooterComponent,
    Footer4Component,
    Footer5Component,
    Footer6Component,
    Footer2Component,
    MagentoDeveloperComponent,
    PhpDeveloperComponent,
    PythonDeveloperComponent,
    Footer1Component,
    RubyOnRailsDeveloperComponent,
    RubyOnRailsDevelopmentComponent,
    AndroidDeveloperComponent,
    IosDeveloperComponent,
    OurClientsComponent,
    SucessStoriesComponent,
    BlogsComponent,
    AutomotiveComponent,
    DigitalMarketingComponent,
    HospitalityComponent,
    MediaEntertainmentComponent,
    RealEstateComponent,
    SocialComputingComponent,
    BankingAndFinancialComponent,
    EducationComponent,
    ItComponent,
    ManufacturingComponent,
    TelecomComponent,
    ConsumerElectronicsComponent,
    GovernmentComponent,
    RetailAndConsumerComponent,
    TravelTransportLogComponent,
    ContactUsComponent,
    LifeScienceAndHealthCareComponent,
    GpsMobileTrakingComponent,
    GpsVehicleTrakingComponent,
    GpsWearablesComponent,
    FieldForceAutomationComponent,
    GpsAssetsTrakingComponent,
    HotelBookingEngineComponent,
    HotelManagementSystemComponent,
    HotelInventoryManagementComponent,
    TravelBookingEcommerceComponent,
    FoodOnWheelComponent,
    ArchitechturalNetworkComponent,
    ConstructionSuppliersComponent,
    EnterpriceCrmSalesComponent,
    EnterpriceCrmSupportComponent,
    SassBasedErpComponent,
    SassBasedPayrollComponent,
    XamarinDevelopmentComponent,
    LocationBasedTechnologyFormobileAppComponent,
    HowIndexingAndDeepLinkingHelpComponent,
    PortfolioSinglepageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [LoginService, SubscribeService,ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
