import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import { ToasterService } from './../toaster-service.service';

declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'app-footer5',
  templateUrl: './footer5.component.html',
  styleUrls: ['./footer5.component.css']
})
export class Footer5Component implements OnInit {
    name: string = "";
    email: string = "";
    subject: string = "";
    message: string = "";
    constructor(public router: Router, public _loginService: LoginService , private toasterService: ToasterService) { }


    Success(){
      this.toasterService.Success("Your message has been sent successfully.")
    }
    Info(){
      this.toasterService.Info("Subscribed")
    }


    jquery_code(){

      $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e) {
          var targeted_popup_class = jQuery(this).attr('data-popup-open');
          $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
      
          e.preventDefault();
        });
      
        //----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
          var targeted_popup_class = jQuery(this).attr('data-popup-close');
          $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
      
          e.preventDefault();
        });
      });
      
           
    }
  

  ngOnInit() {
this.jquery_code();

    $(".featured-carousel").length > 0 && $(".featured-carousel").owlCarousel({
      loop: !0,
      margin: 12,
      responsive: {
          0: {
              items: 1
          },
          600: {
              items: 2
          },
          1e3: {
              items: 3
          }
      }
  



 
  });

}


login() {
    let user = new User('', '','','');
    user.name = this.name;
    user.email = this.email;
     user.subject = this.subject;
    user.message = this.message;

    let data = [
      { 'name': user.name , 'email': user.email ,'subject': user.subject, 'message': user.message }
    ];
    this._loginService.sendLogin({ data })
      .subscribe(
      response => this.handleResponse(response),
      error => this.handleResponse(error)
      );
  }

  handleResponse(response) {
    if (response.success) {
      console.log("success");
    } else if (response.error) {
      console.log("errror");
    } else {

    }

  }

}
