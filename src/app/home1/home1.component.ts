import { FormsModule } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import {NgForm} from '@angular/forms';
import { ToasterService } from './../toaster-service.service';

import { SubscribeService } from './../subscribe.service';



import { Subscribe } from '../subscribe.model';


declare var $: any;
declare var jQuery :any;
@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.css']
})
export class Home1Component implements OnInit {
    name: string = "";
    email: string = "";
    subject: string = "";
    message: string = "";

    constructor(public router: Router, public _loginService: LoginService, public _subscribeService: SubscribeService,
    private toasterService: ToasterService) { }

    Success(){this.toasterService.Success("Your message has been sent successfully.")}
    Info(){
      
      this.toasterService.Info("Subscribed");
  
    
    }

    jquery_code(){

      $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e) {
          var targeted_popup_class = jQuery(this).attr('data-popup-open');
          $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
      
          e.preventDefault();
        });
      
        //----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
          var targeted_popup_class = jQuery(this).attr('data-popup-close');
          $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
      
          e.preventDefault();
        });
      });
      
           
    }
  


    ngOnInit() {

      this.jquery_code();
      jQuery(document).ready(function() {
          jQuery(".materialize-slider").revolution( {
              sliderType:"standard", sliderLayout:"fullwidth", delay:8000, navigation: {
                  keyboardNavigation:"on", keyboard_direction:"horizontal", mouseScrollNavigation:"off", onHoverStop:"on", touch: {
                      touchenabled: "on", swipe_threshold: 75, swipe_min_touches: 1, swipe_direction: "horizontal", drag_block_vertical: !1
                  }
                  , arrows: {
                      style:"gyges", enable:!0, hide_onmobile:!1, hide_onleave:!0, tmp:'', left: {
                          h_align: "left", v_align: "center", h_offset: 10, v_offset: 0
                      }
                      , right: {
                          h_align: "right", v_align: "center", h_offset: 10, v_offset: 0
                      }
                  }
              }
              , responsiveLevels:[1240, 1024, 778, 480], gridwidth:[1140, 1024, 778, 480], gridheight:[400, 300, 500, 500], disableProgressBar:"on", parallax: {
                  type: "mouse", origo: "slidercenter", speed: 2000, levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
              }
          }
          )
      }
      )
  }
  
  login() {
      let user=new User('', '', '', '');
      user.name=this.name;
      user.email=this.email;
      user.subject=this.subject;
      user.message=this.message;
      let data=[ {
          'name': user.name, 'email': user.email, 'subject': user.subject, 'message': user.message
      }
      ];
      this._loginService.sendLogin( {
          data
      }
      ).subscribe(response=>this.handleResponse(response), error=>this.handleResponse(error))
  }
  
  subscribe() {
      let subscribe=new Subscribe('');
      subscribe.email=this.email;
      let data=[ {
          'email': subscribe.email
      }
      ];
      this._subscribeService.sendSubscribe( {
          data
      }
      ).subscribe(response=>this.handleResponse(response),
       error=>this.handleResponse(error));
      console.log(data)
  }
  
  handleResponse(response) {
      if(response.success) {
          console.log("success")
      }
      else if(response.error) {
          console.log("errror")
      }
      else {}
  }


}
