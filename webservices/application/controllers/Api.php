<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
require (APPPATH . '/libraries/REST_Controller.php');


class Api extends REST_Controller 
{


function __construct()
    {
        // Construct the parent class
       
        parent::__construct();
		$this->load->database();
		$this->load->model('Main_model');
       
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }



public function contact_post(){
      $data=array('Name'=>$this->post('name'),
          'Email'=>$this->post('email'),'Subject'=>$this->post('subject'),'Message'=>$this->post('message'));
        $result=$this->Main_model->contact($data);
        


if (isset($result)){
        $message=[
        'Status'=> 'Success',
           ];
            $this->set_response($message, REST_Controller::HTTP_CREATED);  
              
           }else{
           $this->set_response([
            
         'status'=>FALSE,
          'message'=>'failed'
          ],REST_Controller::HTTP_OK);
            
                      
           }
          
       } 


     public function subscribe_post(){
       $data=array('Email'=>$this->post('email'));
        $result=$this->Main_model->subscribe($data);
        


      if (isset($result)){
        $message=[
        'Status'=> 'Success',
           ];
            $this->set_response($message, REST_Controller::HTTP_CREATED);  
              
           }else{
           $this->set_response([
            
         'status'=>FALSE,
          'message'=>'Failed'
          ],REST_Controller::HTTP_OK);
            
                      
           }
          
       } 


    

}